package com.epam.rd.june2018.calc.console;

import com.epam.rd.june2018.calc.core.CalcImpl;

public class App {
    public static void main(String[] args) {

        if (args.length != 3 || args[2].length() != 1) {
            System.out.println("Incorrect arguments!");
        }

        else {
            double a, b, result;
            char operation = args[2].toCharArray()[0];
            CalcImpl calculator = new CalcImpl();

            try {

                a = Double.parseDouble(args[0]);
                b = Double.parseDouble(args[1]);

            }
            catch (Exception ex) {

                System.out.println("Incorrect arguments!");
                return;
            }

            switch (operation) {

                case '+':
                    result = calculator.addition(a, b);
                    break;

                case '-':
                    result = calculator.subtraction(a, b);
                    break;

                case '*':
                    result = calculator.multiplication(a, b);
                    break;

                case '/':
                    result = calculator.division(a, b);
                    break;

                default:
                    System.out.println("Incorrect arguments!");
                    return;
            }

            System.out.println("number1 = " + a + " number2 = " + b + " result = " + result);
        }
    }
}
