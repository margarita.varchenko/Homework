package com.epam.rd.june2018.calc.core;

import com.epam.rd.june2018.calc.interfaces.Calc;

public class CalcImpl implements Calc {

    public double addition(double a, double b) {
        double c = a + b;
        return c;
    }

    public double subtraction(double a, double b) {
        double c = a - b;
        return c;
    }

    public double multiplication(double a, double b) {
        double c = a * b;
        return c;
    }

    public double division(double a, double b) {
        double c = a / b;
        return c;
    }
}