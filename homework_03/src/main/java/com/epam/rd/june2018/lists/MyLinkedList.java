package com.epam.rd.june2018.lists;

public class MyLinkedList<E> extends MyList<E> implements MyQueue<E>, MyDeque<E> {
    private Node head;
    private Node tail;

    public E getHead() {
        return head.value;
    }

    public E getTail() {
        return tail.value;
    }

    public MyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    }

    public E get(int index) {

        if (index < 0  || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        return getNode(index).value;

    }

    private Node getNode(int index) {

        Node currentNode = head;

        for (int i = 0; i < index; i++) {

            if (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            else {
                throw new IndexOutOfBoundsException();
            }

        }

        return currentNode;
    }

    public void add(E item) {

        Node newNode = new Node(item);

        if (head != null) {
            tail.next = newNode;
            newNode.previous = tail;
            tail = newNode;
        }
        else {
            head = newNode;
            tail = newNode;
        }

        size++;
    }

    public void addFirst(E item) {

        Node newNode = new Node(item);

        if (head != null) {
            newNode.next = head;
            head.previous = newNode;
            head = newNode;
        }
        else {
            head = newNode;
            tail = newNode;
        }

        size++;
    }

    public void add(int index, E item) {

        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException();

        if (index == 0) {
            addFirst(item);
            return;
        }

        Node newNode = new Node(item);
        Node nextNode = head;

        for (int i = 0; i < index; i++) {
            nextNode = nextNode.next;
        }

        Node prevNode = nextNode.previous;

        prevNode.next = newNode;
        newNode.previous = prevNode;
        nextNode.previous = newNode;
        newNode.next = nextNode;

        size++;
    }

    public E deleteTail() {
        if (head != null) {
            E deletedItem = getTail();
            if (head != tail) {
                tail = tail.previous;
                tail.next = null;
                size--;
            }
            else {
                clear();
            }
            return deletedItem;
        }
        else {
            throw new NullPointerException();
        }
    }

    public E deleteHead() {
        if (head != null) {
            E deletedItem = getHead();
            if (size != 1) {
                head = head.next;
                head.previous = null;
                size--;
            }
            else {
                clear();
            }
            return deletedItem;
        }
        else {
            throw new NullPointerException();
        }
    }

    public E delete(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (head != null) {

            if (index == 0) {
                return deleteHead();
            }
            if (index == size - 1) {
                return deleteTail();
            }

            E deletedItem = get(index);
            Node previousNode = getNode(index-1);
            Node nextNode = getNode(index + 1);
            previousNode.next = nextNode;
            nextNode.previous = previousNode;

            size--;

            return deletedItem;
        }
        else {
            throw new NullPointerException();
        }
    }

    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    private class Node {
        private E value;
        private Node previous;
        private Node next;

        public Node() { }

        public Node(E value) {
            this.value = value;
        }
    }
}
