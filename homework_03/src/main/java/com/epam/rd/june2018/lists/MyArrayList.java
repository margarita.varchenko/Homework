package com.epam.rd.june2018.lists;

public class MyArrayList<E> extends MyList<E> {
    private E [] arrayList;

    public E get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return arrayList[index];
    }

    public MyArrayList() {
        arrayList = (E[]) new Object[10];
    }

    public MyArrayList(int initialCapacity) {
        arrayList = (E[]) new Object[initialCapacity];
    }

    public void add(E item) {
        if (size() != arrayList.length) {
            arrayList[size] = item;
        }
        else {
            E[] tmpArrayList = arrayList;
            arrayList = (E[]) new Object[size + 1];
            System.arraycopy(tmpArrayList, 0, arrayList, 0, tmpArrayList.length);
            arrayList[arrayList.length - 1] = item;
        }
        size++;
    }

    public void add(int index, E item) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        else {
            if (size == arrayList.length) {
                E[] tmpArray = (E[]) new Object[size + 1];
                System.arraycopy(arrayList, 0, tmpArray, 0, size);
                arrayList = tmpArray;
            }
            System.arraycopy(arrayList, index, arrayList, index + 1, size - index);
            arrayList[index] = item;

            size++;
        }
    }

    public E delete(int index) {

        E deletedItem = get(index);

        if (index != size - 1) {
            System.arraycopy(arrayList, index + 1, arrayList, index, size - index - 1);
        }

        size--;

        arrayList[size] = null;

        return deletedItem;
    }

}
