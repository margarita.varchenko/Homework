package com.epam.rd.june2018.lists;

public interface MyDeque<E> extends MyQueue<E> {

    void addFirst(E item);
    E deleteTail();
    E getTail();

}
