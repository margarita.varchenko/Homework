package com.epam.rd.june2018.deadlock;

public class Deadlock {
    public static void main(String[] args) {
        System.out.println("Start main thread");

        SomeObject object1 = new SomeObject("object1");
        SomeObject object2 = new SomeObject("object2");

        MyThread myThread1 = new MyThread(object1, object2, "myThread1");
        Thread thread1 = new Thread(myThread1, "thread1");


        MyThread myThread2 = new MyThread(object2, object1, "myThread2");
        Thread thread2 = new Thread(myThread2, "thread2");

        thread1.start();
        thread2.start();

    }

}
