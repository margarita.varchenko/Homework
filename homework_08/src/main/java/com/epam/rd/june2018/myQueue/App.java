package com.epam.rd.june2018.myQueue;

public class App {
    public static void main(String[] args) throws InterruptedException {
        MyQueue queue = new MyQueue();
        Consumer consumer = new Consumer(queue);
        consumer.start();
        new Producer(queue);
        Thread.sleep(500);
        consumer.interrupt();
    }

}


