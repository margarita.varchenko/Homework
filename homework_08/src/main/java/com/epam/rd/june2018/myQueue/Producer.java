package com.epam.rd.june2018.myQueue;

public class Producer implements Runnable {
    private MyQueue queue;
    public Producer(MyQueue queue) {
        this.queue = queue;
        new Thread(this, "Producer").start();
    }

    public void run() {
        for (int i = 1; i <= 5; i++) {
            queue.put(i);
        }
}
}