package com.epam.rd.june2018.myQueue;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MyQueue {
    private int value;
    private boolean valueSet = false;

    private ReentrantLock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    private void lock() {
        lock.lock();
    }

    public void put(int value) {
        lock.lock();
        try {
            while (valueSet) {
                condition.await();
            }
            this.value = value;
            valueSet = true;
            System.out.println("Sent: " + value);
            condition.signal();
        }
        catch (InterruptedException ex) {
            System.out.println("Thread " + Thread.currentThread().getName() + " was interrupted in put.");
        }
        finally {
            lock.unlock();
        }

    }

    public int get() {

        lock.lock();
        try {
            while (!valueSet) {
                condition.await();
            }
            valueSet = false;

            System.out.println("Received: " + value);
            condition.signal();

        }
        catch (InterruptedException ex) {
            System.out.println("Get interruption was handled");
            Thread.currentThread().interrupt();
            //return 0;
        }

        finally {
            lock.unlock();
        }
        return value;
    }

}
