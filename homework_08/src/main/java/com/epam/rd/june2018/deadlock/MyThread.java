package com.epam.rd.june2018.deadlock;

public class MyThread implements Runnable {
    private String threadName;
    private static SomeObject object1;
    private static SomeObject object2;

    MyThread(SomeObject obj1, SomeObject obj2, String name) {
        object1 = obj1;
        object2 = obj2;
        threadName = name;
    }

    public void run() {
        System.out.println(threadName + " - run");

            object1.doSomething(object2);
            object2.doSomething(object1);

        System.out.println(threadName + " - end");
    }
}
