package com.epam.rd.june2018.singletone;

// Singelton class
public class TheOne {
    private static TheOne instance;

    private String hisName;

    private TheOne() {
        hisName = "Neo";
    }

    public static synchronized TheOne getInstance() {
        if (instance == null) {
            instance = new TheOne();
        }
        return instance;
    }

    public String getHisName() {
        return hisName;
    }

}
