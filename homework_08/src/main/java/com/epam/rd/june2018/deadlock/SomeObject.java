package com.epam.rd.june2018.deadlock;

public class SomeObject {
    private String objectName;

    public SomeObject(String name) {
        objectName = name;
    }
    public synchronized void doSomething(SomeObject object) {
        System.out.println("lock object " + objectName);
        try {
            Thread.sleep(400);
        }
        catch (InterruptedException ex) {
            System.out.println("Interrupted.");
        }
        object.doSomethingElse();
    }

    public synchronized void doSomethingElse() {
        System.out.println("Doing something else with " + objectName + " by " + Thread.currentThread().getName());
    }
}
