There is a program for parsing log files.
After execution of mvn package command an executable jar logparse-jar-with-dependencies.jar will be created.
The jar file can be executed with arguments (the list is printed if jar is executed with no arguments):
 -c (--consoleOutput)   : Print results to console.
 -i (--inputFile) VAL   : Path and name of file to read results from.
 -l (--logfile) VAL     : Path and name of file with logs.
 -n (--entriesNumber) N : Number of the longest operations in each module to be
                          output.
 -o (--outputFile) VAL  : Path and name of file to write results in.

 For example:
    -c -l logs.txt -n 5
        prints 5 longest operations from each module of file log.txt
    -o results.dat -l logs.txt -n 10
        saves 10 longest operations from each module of file log.txt to file results.dat
    -i results.dat
        prints results from file results.dat