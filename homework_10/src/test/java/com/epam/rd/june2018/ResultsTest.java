package com.epam.rd.june2018;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ResultsTest {
    @Test
    public void setResultsTest() throws Exception {
        // GIVEN
        Results results = new Results();
        String logfile = "logs.txt";
        int n = 5;

        // WHEN
        results.setResults(logfile, n);

        // THEN
        for (Map.Entry<String, OperationInfo[]> entry:results) {
            Assert.assertEquals(n, entry.getValue().length);
        }
    }

}