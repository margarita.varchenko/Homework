package com.epam.rd.june2018;


import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.*;

import static java.lang.System.out;


public class Main {

    @Option(name="-c", aliases="--consoleOutput", usage="Print results to console.", depends = {"-l"})
    private boolean consoleOutput;

    @Option(name="-l", aliases="--logfile", usage="Path and name of file with logs.", forbids={"-i"}, depends = {"-n"})
    private String logfile;

    @Option(name="-i", aliases="--inputFile", usage="Path and name of file to read results from.", forbids={"-l", "-o"})
    private String inputFile;

    @Option(name="-o", aliases="--outputFile", usage="Path and name of file to write results in.", depends = {"-l"}, forbids = {"-i"})
    private String outputFile;

    @Option(name="-n", aliases="--entriesNumber", usage="Number of the longest operations in each module to be output.")
    private int n;

    private void doMain(final String[] arguments)  {

        parseArguments(arguments);

        if (logfile != null) {

            Results results = getResults();

            if (consoleOutput) {
                System.out.println(results);
            }

            if (outputFile != null) {
                try (FileOutputStream fos = new FileOutputStream(outputFile);
                     ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                    oos.writeObject(results);
                }
                catch (IOException ioEx) {
                    System.out.println("Failed to open or read file.");
                    return;
                }
                return;
            }

        }

        if (inputFile != null) {
            readInputFile();
        }
    }

    private void parseArguments(String... arguments) {
        final CmdLineParser parser = new CmdLineParser(this);
        if (arguments.length < 1)
        {
            parser.printUsage(out);
            return;
        }
        try
        {
            parser.parseArgument(arguments);
        }
        catch (CmdLineException cmdEx)
        {
            out.println("ERROR: Unable to parse command-line options: " + cmdEx);
        }
    }

    private Results getResults() {
        Results results = new Results();
        try {
            results.setResults(logfile, n);
        }
        catch (IOException ioEx) {
            System.out.println("Failed to open or read file.");
        }
        return results;
    }

    private void readInputFile() {
        Results results;
        try (FileInputStream fis = new FileInputStream(inputFile);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            results = (Results) ois.readObject();
        } catch (ClassNotFoundException ex) {
            System.out.println("Failed to read object.");
            return;
        }
        catch (IOException ioEx) {
            System.out.println("Failed to open or read file.");
            return;
        }
        System.out.println(results.toString());
    }
    /**
     * Executable function demonstrating Args4j command-line processing.
     *
     * @param arguments Command-line arguments to be processed with Args4j.
     */
    public static void main(final String... arguments)
    {
        final Main instance = new Main();

        instance.doMain(arguments);

    }
}