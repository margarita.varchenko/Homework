package com.epam.rd.june2018;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.Files.lines;

public class Results implements java.io.Serializable, Iterable<Map.Entry<String, OperationInfo[]>> {

    private Map<String, OperationInfo[]> moduleOperationsPairs;

    public Map<String, OperationInfo[]> getModuleOperationsPairs() {
        return moduleOperationsPairs;
    }

    void setResults(String filename, int n) throws IOException {
        Path path = Paths.get(filename);

        List<String> modules = getModules(path);

        Stream<OperationInfo> operations;
        Stream<String> lines = null;
        moduleOperationsPairs = new HashMap<>();
        for (String module : modules) {
            try {
                lines = lines(path);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            operations = lines.map(Results::lineToOperation).sorted(Comparator.reverseOrder()).filter(operation -> operation.getModule().equals(module)).limit(n);
            moduleOperationsPairs.put(module, operations.toArray(OperationInfo[]::new));
        }

    }

    private static OperationInfo lineToOperation(String line) {
        Pattern linePattern = Pattern.compile("^(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\.\\d{3}).+Module=(\\w+)\\s+Operation: (.+)\\b\\s+Execution time: (\\d+)\\s+ms");
        Matcher matcher = linePattern.matcher(line);
        OperationInfo operation = new OperationInfo();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.S");
        String endTimeStr = "";
        if (!matcher.matches()) {
            throw new IllegalStateException("Line format does not match.");
        }
        endTimeStr = matcher.group(1);
        try {
            operation.setEndTime(simpleDateFormat.parse(endTimeStr));
        }
        catch (ParseException ex) {
            System.out.println("Fail: Wrong line format");
        }

        operation.setModule(matcher.group(2));

        operation.setOperationName(matcher.group(3));

        operation.setExecutionTime(Integer.parseInt(matcher.group(4)));

        return operation;
    }

    private List<String> getModules(Path path) throws IOException {
        if (!Files.exists(path)) {
            throw new IOException();
        }
        Stream<String> lines = null;
        try {
            lines = lines(path);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return lines.map(Results::lineToOperation).map(OperationInfo::getModule).distinct().collect(Collectors.toList());
    }

    public Iterator<Map.Entry<String, OperationInfo[]>> iterator() {
        return moduleOperationsPairs.entrySet().iterator();
    }

    @Override
    public String toString() {
        return moduleOperationsPairs.entrySet().stream().map(this::entryToString).collect( Collectors.joining( "\n" ));
    }

    private String entryToString(Map.Entry<String, OperationInfo[]> entry) {
        String result = entry.getKey() + " Module:\n";
        StringBuilder stringBuilder = new StringBuilder(result);

        for (OperationInfo operation:entry.getValue()) {
            result = stringBuilder.append("\t")
                    .append(operation.getOperationName()).append(" ")
                    .append(operation.getExecutionTime())
                    .append(" ms, finished at ").append(operation.getEndTime())
                    .append("\n").toString();
        }
        return result;
    }
}
