package com.epam.rd.june2018;

import java.io.Serializable;
import java.util.Date;

public class OperationInfo implements Comparable<OperationInfo>, Serializable {
    private String operationName;
    private String module;
    private int executionTime;
    private Date endTime;

    public String getOperationName() {
        return operationName;
    }

    public int getExecutionTime() {
        return executionTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public String getModule() {
        return module;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public void setExecutionTime(int executionTime) {
        this.executionTime = executionTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public int compareTo(OperationInfo operation) {
        return Integer.compare(executionTime, operation.executionTime);
    }
}
