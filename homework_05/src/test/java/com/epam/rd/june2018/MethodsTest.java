package com.epam.rd.june2018;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class MethodsTest {
    Methods methods = new Methods();

    @Test
    public void intListToStringTest() {
        // GIVEN
        List<Integer> intList = new ArrayList<Integer>(5);
        intList.add(9);
        intList.add(-4);
        intList.add(0);
        intList.add(35);
        intList.add(100);
        String expectedString = "o9,e-4,e0,o35,e100";

        // WHEN
        String result = methods.intListToString(intList);

        // THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(expectedString, result);

    }

    @Test
    public void getLargestCityTest() {
        // GIVEN
        City tokio = new City("Japan", 13513734),
                deli = new City("India", 12567468),
                pekin = new City("China", 10867642),
                kalkutta = new City("India", 19567356),
                kioto = new City("Japan", 24336563),
                jaipur = new City("India", 9566335),
                shanhay =  new City("China", 24876329),
                nankin = new City("China", 11234566),
                osaka = new City("Japan", 11356745),
                hirosima = new City("Japan", 10564564);
        City[] cityArray = new City[] {tokio, deli, pekin, kalkutta,
                kioto, jaipur,shanhay, nankin, osaka, hirosima};
        Collection<City> cities = Arrays.asList(cityArray);
        Map<String, City> expectedMap = new HashMap<>();

        // WHEN
        Map<String, City> largestCitiesMap = methods.getLargestCityPerState(cities);
        // THEN
        Assert.assertNotNull(largestCitiesMap);
        Assert.assertEquals(kioto, largestCitiesMap.get("Japan"));
        Assert.assertEquals(kalkutta, largestCitiesMap.get("India"));
        Assert.assertEquals(shanhay, largestCitiesMap.get("China"));
    }

    @Test
    public void zipTest() {
        // GIVEN
        List<Integer> firstList = new ArrayList<>(4),
                secondList = new LinkedList<>();
        firstList.add(-9);
        firstList.add(-3);
        firstList.add(-2);
        firstList.add(-299);

        secondList.add(4);
        secondList.add(9);
        secondList.add(3);

        Stream<Integer> firstStream = firstList.stream(),
                secondStream = secondList.stream();

        int expectedSize = (firstList.size() < secondList.size() ? firstList.size() : secondList.size()) * 2;

        // WHEN
        Stream<Integer> result = Methods.zip(firstStream, secondStream);
        // THEN
        Assert.assertNotNull(result);

        firstStream = firstList.stream();
        secondStream = secondList.stream();
        Iterator firstIter = firstStream.iterator(),
                secondIter = secondStream.iterator(),
                resultIter = result.iterator();

        while(firstIter.hasNext() && secondIter.hasNext()) {
            Assert.assertEquals(firstIter.next(), resultIter.next());
            Assert.assertEquals(secondIter.next(), resultIter.next());
        }
    }

    @Test
    public void booksListTest() {
        // GIVEN
        BooksList listOfBooks = new BooksList();
        Book b0 = new Book("Hamlet", "Shakespeare", 4562476357986L);
        Book b1 = new Book("Romeo and Juliette", "Shakespeare", 7687653454564L);
        Book b2 = new Book("11.22.63", "King", 5674356789872L);
        Book b3 = new Book("Peace and war", "Tolstoy", 7654356789876L);
        Book b4 = new Book("A Game of Thrones", "Martin", 8768976543452L);
        Book b5 = new Book("A Dance with Dragons", "Martin", 7896543452341L);
        Book b6 = new Book("A Storm of Swords", "Martin", 7896543456782L);
        listOfBooks.addBook(b0);
        listOfBooks.addBook(b1);
        listOfBooks.addBook(b2);
        listOfBooks.addBook(b3);
        listOfBooks.addBook(b4);
        listOfBooks.addBook(b5);
        listOfBooks.addBook(b6);

        // WHEN
        Map<String, Integer> authorBooksNumber = listOfBooks.getMapAuthorsBooksNumber();

        // THEN
        Assert.assertEquals(2, (int)authorBooksNumber.get("Shakespeare"));
        Assert.assertEquals(1, (int)authorBooksNumber.get("King"));
        Assert.assertEquals(3, (int)authorBooksNumber.get("Martin"));
    }
}