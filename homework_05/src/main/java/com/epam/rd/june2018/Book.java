package com.epam.rd.june2018;

public class Book {
    private String title;
    private String author;
    private long isbn;

    public Book(String bookTitle, String bookAuthor, long bookIsbn) {
        title = bookTitle;
        author = bookAuthor;
        isbn = bookIsbn;
    }

    public String getAuthor() {
        return author;
    }
}
