package com.epam.rd.june2018;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Methods {

    public String intListToString(List<Integer> integerList) {
        String result;

        result = integerList.stream()
                .map(i -> ((i % 2 == 0 ? "e" : "o") + i.toString()))
                .collect(Collectors.joining(","));

        return result;
    }


    Map<String, City> getLargestCityPerState(Collection<City> cities) {

        Comparator<City> comparator = Comparator.comparing(City::getPopulation);
        Map<String, List<City>> groupedCities = cities.stream().collect(Collectors.groupingBy(City::getState));
        Stream<Map.Entry<String, List<City>>> streamOfEntries = groupedCities.entrySet().stream();
        Stream<Map.Entry<String, City>> streamOfLargestCities = streamOfEntries.map(entry ->
                new AbstractMap.SimpleEntry<String, City>(entry.getKey(), Collections.max(entry.getValue(), comparator)));

        Map<String, City> result = groupedCities.entrySet().stream().map(entry -> new AbstractMap.SimpleEntry<String, City>(entry.getKey(), Collections.max(entry.getValue(), comparator)))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
        return result;
    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {


        Iterator<T> firstIter = first.iterator(),
                secondIter = second.iterator();

        List<T> resultList = new ArrayList<T>();

        while (firstIter.hasNext() && secondIter.hasNext()) {
            resultList.add(firstIter.next());
            resultList.add(secondIter.next());
        }

        return resultList.stream();
    }

}
