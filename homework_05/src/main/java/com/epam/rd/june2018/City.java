package com.epam.rd.june2018;

import java.util.Comparator;

public class City {
    private String state;
    private long population;

    public City(String cityState, long cityPopulation) {
        state = cityState;
        population = cityPopulation;
    }

    public String getState() {
        return state;
    }

    public long getPopulation() {
        return population;
    }

    public static Comparator<City> getComparator() {
        return Comparator.comparing(City::getPopulation);
    }
}
