package com.epam.rd.june2018;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BooksList {
    private List<Book> booksList;

    public BooksList() {
        booksList = new ArrayList<>();
    }

    public void addBook(Book book) {
        booksList.add(book);
    }

    public Map<String, Integer> getMapAuthorsBooksNumber() {
        Map<String, List<Book>> groupedBooks = booksList.stream().collect(Collectors.groupingBy(Book::getAuthor));

        return groupedBooks.entrySet().stream().map(entry -> new AbstractMap.SimpleEntry<String, Integer>(entry.getKey(), entry.getValue().size()))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }
}
