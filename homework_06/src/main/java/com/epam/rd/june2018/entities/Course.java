package com.epam.rd.june2018.entities;

import java.util.Date;

public class Course {
    private long id;
    private String title;
    private Date startDate;
    private int durationWeeks;

    public Course(int courseID, String courseTitle, Date courseStartDate, int courseDurationInWeeks) {
        id = courseID;
        title = courseTitle;
        startDate = courseStartDate;
        durationWeeks = courseDurationInWeeks;
    }

    public Course(String courseTitle, Date courseStartDate, int courseDurationInWeeks) {
        title = courseTitle;
        startDate = courseStartDate;
        durationWeeks = courseDurationInWeeks;
    }

    public Course() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getDurationWeeks() {
        return durationWeeks;
    }

    public void setDurationWeeks(int durationWeeks) {
        this.durationWeeks = durationWeeks;
    }
}
