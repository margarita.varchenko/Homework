package com.epam.rd.june2018.DAO;

import com.epam.rd.june2018.entities.Student;

import java.sql.*;
import java.util.Iterator;
import java.util.List;

public class StudentDAO extends AbstractDAO<Student,String> {

    @Override
    protected String getSelectByKeyQuery() {
        return "SELECT * FROM students WHERE email = ?";
    }

    @Override
    protected String getSelectAllQuery() {
        return "SELECT * FROM students";
    }
    @Override
    protected String getSelectByAllFieldsQuery() {
        return "SELECT * FROM students WHERE email = ? AND name = ? AND password = ?";
    }

    @Override
    protected String getSelectByKeysQuery(List<String> listOfKeys) {
        Iterator<String> iterator = listOfKeys.iterator();
        String selectByEmailQuery = "SELECT * FROM students WHERE email = '" + iterator.next() + "' ";
        StringBuilder stringBuilder = new StringBuilder(selectByEmailQuery);
        while (iterator.hasNext()) {
            stringBuilder.append(" OR email = '");
            stringBuilder.append(iterator.next());
            stringBuilder.append("' ");
        }
        return stringBuilder.toString();
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO students (email, name, password) VALUES (?, ?, ?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE students SET name = ?, password = ? WHERE email = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM students WHERE email = ?";
    }

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Student newStudent) throws SQLException {
        statement.setString(1, newStudent.getEmail());
        statement.setString(2, newStudent.getName());
        statement.setString(3, newStudent.getPassword());
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Student updatedStudent) throws SQLException {
        statement.setString(1, updatedStudent.getName());
        statement.setString(2, updatedStudent.getPassword());
        statement.setString(3, updatedStudent.getEmail());
    }

    @Override
    protected void prepareSelectByKeyStatement(PreparedStatement statement, String key) throws SQLException {
        statement.setString(1, key);
    }

    @Override
    protected void prepareSelectByAllFieldsStatement(PreparedStatement statement, Student item) throws SQLException {
        statement.setString(1, item.getEmail());
        statement.setString(2, item.getName());
        statement.setString(3, item.getPassword());
    }


    @Override
    protected void prepareDeleteStatement(PreparedStatement statement, String key) throws SQLException {
        statement.setString(1, key);
    }

    @Override
    protected Student getNextFromResultSet(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setEmail(resultSet.getString("email"));
        student.setName(resultSet.getString("name"));
        student.setPassword(resultSet.getString("password"));
        return student;
    }

    /*
    Does nothing because table 'students' does not have
    autoincrement primary key
     */
    @Override
    protected String getActualKey(Student insertedItem, PreparedStatement statement) {
        return insertedItem.getEmail();
    }

}
