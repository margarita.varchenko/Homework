package com.epam.rd.june2018.DAO;

import com.epam.rd.june2018.entities.Course;

import java.sql.*;
import java.util.Iterator;
import java.util.List;

public class CourseDAO extends AbstractDAO<Course,Long> {

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Course newCourse) throws SQLException {
        statement.setString(1, newCourse.getTitle());
        statement.setDate(2, convert(newCourse.getStartDate()));
        statement.setInt(3, newCourse.getDurationWeeks());
    }

    @Override
    protected String getSelectByKeyQuery() {
        return "SELECT * FROM courses WHERE id = ?";
    }

    @Override
    protected String getSelectAllQuery() {
        return "SELECT * FROM courses";
    }

    /*
    Does not check the id: only rows with the same title, start date and duration in
    weeks will be selected.
     */
    protected String getSelectByAllFieldsQuery() {
        return "SELECT * FROM courses WHERE title = ? AND start_date = ? AND duration_weeks = ?";
    }

    @Override
    protected String getSelectByKeysQuery(List<Long> listOfKeys) {
        Iterator<Long> iterator = listOfKeys.iterator();
        String selectByEmailQuery = "SELECT * FROM courses WHERE id = " + iterator.next().toString();
        StringBuilder stringBuilder = new StringBuilder(selectByEmailQuery);
        while (iterator.hasNext()) {
            stringBuilder.append(" OR id = ");
            stringBuilder.append(iterator.next().toString());
        }

        return stringBuilder.toString();
    }

    @Override
    protected String getInsertQuery() {
        return "INSERT INTO courses (title, start_date, duration_weeks) VALUES (?, ?, ?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE courses SET title = ?, start_date = ?, duration_weeks = ? WHERE id = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM courses WHERE id = ?";
    }

    @Override
    protected void prepareSelectByKeyStatement(PreparedStatement statement, Long id) throws SQLException {
        statement.setLong(1, id);
    }

    @Override
    protected void prepareDeleteStatement(PreparedStatement statement, Long key) throws SQLException {
        statement.setLong(1, key);
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Course updatedItem) throws SQLException {
        statement.setString(1, updatedItem.getTitle());
        statement.setDate(2, convert(updatedItem.getStartDate()));
        statement.setInt(3, updatedItem.getDurationWeeks());
        statement.setLong(4, updatedItem.getId());
    }

    @Override
    protected void prepareSelectByAllFieldsStatement(PreparedStatement statement, Course item) throws SQLException {
        statement.setString(1, item.getTitle());
        statement.setDate(2, convert(item.getStartDate()));
        statement.setInt(3, item.getDurationWeeks());
    }

    @Override
    protected Course getNextFromResultSet(ResultSet resultSet) throws SQLException {
        Course course = new Course();
        course.setId(resultSet.getInt("id"));
        course.setTitle(resultSet.getString("title"));
        course.setStartDate(resultSet.getDate("start_date"));
        course.setDurationWeeks(resultSet.getInt("duration_weeks"));
        return course;
    }

    @Override
    protected Long getActualKey(Course insertedItem, PreparedStatement statement) throws SQLException {
        ResultSet actualKey = statement.getGeneratedKeys();
        if (actualKey.next()) {
            return actualKey.getLong(1);
        }
        else {
            throw new SQLException("Failed to get actual key.");
        }
    }



}
