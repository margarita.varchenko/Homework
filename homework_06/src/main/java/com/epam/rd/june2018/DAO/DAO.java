package com.epam.rd.june2018.DAO;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface DAO<E,K> {
    K insert(E newItem) throws SQLException, IOException;

    void delete(K key) throws SQLException, IOException;

    E getByKey(K key) throws SQLException, IOException;

    List<E> getAll() throws SQLException, IOException;

    void update(E updatedItem) throws SQLException, IOException;

    List<K> insert(List<E> listOfItems) throws SQLException, IOException;

    List<E> getByKey(List<K> listOfKeys) throws SQLException, IOException;

    void delete(List<K> listOfKeys) throws SQLException, IOException;

    boolean exists(E item) throws SQLException, IOException;

    boolean keyExists(K key) throws SQLException, IOException;
}
