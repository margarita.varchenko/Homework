package com.epam.rd.june2018.DAO;

import com.epam.rd.june2018.entities.Course;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class CourseDAOTest {
    DAO<Course,Long> courseDAO = new CourseDAO();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void insertTest() throws SQLException, IOException {
        // GIVEN
        Date date = new Date(2018, 6, 20);
        Course course = new Course("Android dev", date, 10);

        // WHEN

        long id = courseDAO.insert(course);

        // THEN
        Assert.assertEquals(true, courseDAO.exists(course));
        Course gottenCourse = courseDAO.getByKey(id);
        Assert.assertEquals("Android dev", gottenCourse.getTitle());
        Assert.assertEquals(date, gottenCourse.getStartDate());
        Assert.assertEquals(10, gottenCourse.getDurationWeeks());
    }

    @Test
    public void updateTest() throws SQLException, IOException {
        // GIVEN
        Course course = new Course("Algorithms", new Date(2018, 9, 12), 12);
        long id = courseDAO.insert(course);
        course.setId(id);

        course.setTitle("Algorithms and data structure"); // update course

        // WHEN
        courseDAO.update(course);

        // THEN
        Assert.assertEquals(true, courseDAO.keyExists(id));
        Assert.assertEquals(true, courseDAO.exists(course));
        Assert.assertEquals("Algorithms and data structure", courseDAO.getByKey(id).getTitle());
        Assert.assertEquals(12, courseDAO.getByKey(id).getDurationWeeks());
    }

    @Test
    public void deleteTest() throws SQLException, IOException {
        // GIVEN
        List<Course> allCourses = courseDAO.getAll();
        Course firstCourse = allCourses.get(0);

        // WHEN
        courseDAO.delete(firstCourse.getId());

        // THEN
        Assert.assertEquals(false, courseDAO.keyExists(firstCourse.getId()));
        allCourses.remove(0);
        for (Course course:allCourses) {
            Assert.assertEquals(true, courseDAO.keyExists(course.getId()));
        }
    }

    @Test
    public void insertAndGetFewTest() throws SQLException, IOException {
        // GIVEN
        List<Course> coursesList = new ArrayList<>(3);
        coursesList.add(new Course("IOs dev", new Date(2018, 7, 15), 20));
        coursesList.add(new Course("PHP getting started", new Date(2018, 8, 15), 5));
        coursesList.add(new Course("C#", new Date(2018, 7, 21), 20));

        // WHEN
        List<Long> actualKeys = courseDAO.insert(coursesList);
        List<Course> gottenCourses = courseDAO.getByKey(actualKeys);

        // THEN
        for (int i = 0; i < actualKeys.size(); i++) {
            Assert.assertEquals(coursesList.get(i).getTitle(), gottenCourses.get(i).getTitle());
            Assert.assertEquals(coursesList.get(i).getStartDate(), gottenCourses.get(i).getStartDate());
            Assert.assertEquals(coursesList.get(i).getDurationWeeks(), gottenCourses.get(i).getDurationWeeks());
        }
    }

    @Test
    public void deleteFewTest() throws SQLException, IOException {
        // GIVEN
        List<Course> allCourses = courseDAO.getAll();
        List<Course> coursesList = new ArrayList<>(3);
        coursesList.add(new Course("IOs dev", new Date(2018, 7, 15), 20));
        coursesList.add(new Course("PHP getting started", new Date(2018, 8, 15), 5));
        coursesList.add(new Course("C#", new Date(2018, 7, 21), 20));

        List<Long> actualKeys = courseDAO.insert(coursesList);

        // WHEN
        courseDAO.delete(actualKeys);

        // THEN
        List<Course> remainingCourses = courseDAO.getAll();
        for (long key:actualKeys) {
            Assert.assertEquals(false, courseDAO.keyExists(key));
        }
        Assert.assertEquals(allCourses.size(), remainingCourses.size());
    }

}