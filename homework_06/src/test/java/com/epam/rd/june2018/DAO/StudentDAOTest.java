package com.epam.rd.june2018.DAO;

import com.epam.rd.june2018.DAO.DAO;
import com.epam.rd.june2018.DAO.StudentDAO;
import com.epam.rd.june2018.entities.Student;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDAOTest {
    DAO<Student,String> studentDAO = new StudentDAO();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void createAndReadTest() throws SQLException, IOException{
        // GIVEN
        String email = "firstStudent@gmail.com";
        Student student = new Student(email, "james", "7775554440");

        // WHEN
        if (studentDAO.keyExists(email)) {
            studentDAO.delete(email);
        }
        studentDAO.insert(student);
        Student readStudent = studentDAO.getByKey(email);

        // THEN
        Assert.assertNotNull(readStudent);
        Assert.assertEquals(student.getEmail(), readStudent.getEmail());
        Assert.assertEquals(student.getName(), readStudent.getName());
        Assert.assertEquals(student.getPassword(), student.getPassword());

    }

    @Test
    public void updateTest() throws SQLException, IOException {
        // GIVEN
        String email1 = "firstStudent@gmail.com",
                email2 = "secondStudent@gmail.com";
        Student studentToUpdate = new Student(email1, "james bond", "7775554440"),
                nonUpdateStudent = new Student(email2, "must not be updated", "2222");

        if (!studentDAO.keyExists(email1)) {
            studentDAO.insert(new Student(email1, "not updated", "1111"));
        }
        if (!studentDAO.keyExists(email2)) {
            studentDAO.insert(nonUpdateStudent);
        }

        // WHEN
        studentDAO.update(studentToUpdate);
        Student readStudent = studentDAO.getByKey("firstStudent@gmail.com");

        // THEN
        Assert.assertNotNull(readStudent);
        Assert.assertEquals(studentToUpdate.getEmail(), readStudent.getEmail());
        Assert.assertEquals(studentToUpdate.getName(), readStudent.getName());
        Assert.assertEquals(studentToUpdate.getPassword(), studentToUpdate.getPassword());

        Assert.assertNotEquals(studentToUpdate.getName(), nonUpdateStudent.getName());
        Assert.assertNotEquals(studentToUpdate.getPassword(), nonUpdateStudent.getPassword());
    }

    @Test
    public void deleteTest() throws SQLException, IOException {
        // GIVEN
        String email = "firstStudent@gmail.com";
        Student student = new Student(email, "james bond", "7775554440");
        if (!studentDAO.keyExists(email)) {
            studentDAO.insert(student);
        }

        // WHEN
        studentDAO.delete(email);

        // THEN
        Assert.assertEquals(false, studentDAO.keyExists(email));

    }

    @Test
    public void createAndReadFewTest() throws SQLException, IOException {
        // GIVEN
        List<String> emailsList = new ArrayList<>(3);
        emailsList.add("st1@gmail.com");
        emailsList.add("st2@gmail.com");
        emailsList.add("st3@gmail.com");
        Student st1 = new Student(emailsList.get(0), "st1", "1111");
        Student st2 = new Student(emailsList.get(1), "st2", "2222");
        Student st3 = new Student(emailsList.get(2), "st3", "3333");

        List<Student> studentsList = new ArrayList<Student>(3);
        studentsList.add(st1);
        studentsList.add(st2);
        studentsList.add(st3);

        for (int i = 0; i < emailsList.size(); i++) {
            if (studentDAO.keyExists(emailsList.get(i))) {
                studentDAO.delete(emailsList.get(i));
            }
        }

        // WHEN
        studentDAO.insert(studentsList);
        List<Student> readList = studentDAO.getByKey(emailsList);

        // THEN
        Assert.assertNotNull(readList);
        for (int i = 0; i < studentsList.size(); i++) {
            Assert.assertEquals(studentsList.get(i).getEmail(), readList.get(i).getEmail());
            Assert.assertEquals(studentsList.get(i).getName(), readList.get(i).getName());
            Assert.assertEquals(studentsList.get(i).getPassword(), readList.get(i).getPassword());
        }

    }

    @Test
    public void deleteFewTest() throws SQLException, IOException {
        // GIVEN
        List<String> emailsList = new ArrayList<>(3);
        emailsList.add("st1@gmail.com");
        emailsList.add("st2@gmail.com");
        emailsList.add("st3@gmail.com");
        Student st1 = new Student(emailsList.get(0), "st1", "1111");
        Student st2 = new Student(emailsList.get(1), "st2", "2222");
        Student st3 = new Student(emailsList.get(2), "st3", "3333");

        List<Student> studentsList = new ArrayList<Student>(3);
        studentsList.add(st1);
        studentsList.add(st2);
        studentsList.add(st3);

        for (int i = 0; i < emailsList.size(); i++) {
            if (!studentDAO.keyExists(emailsList.get(i))) {
                studentDAO.insert(studentsList.get(i));
            }
        }

        // WHEN
        studentDAO.delete(emailsList);

        // THEN
        for (int i = 0; i < emailsList.size(); i++) {
            Assert.assertEquals(false, studentDAO.keyExists(emailsList.get(i)));
        }
    }

    @Test
    public void insertException() throws SQLException, IOException {
        // GIVEN
        expectedException.expect(BatchUpdateException.class);
        studentDAO.insert(new Student("emailToDuplicate@gmail.com", "student1", "8888"));

        // WHEN
        studentDAO.insert(new Student("emailToDuplicate@gmail.com", "student2", "0000"));

    }


}