package com.epam.rd.june2018.argumentParser;

import java.io.IOException;

public class UnzipArgumentParser extends ArgumentParser {
    @Override
    protected void checkExtensions(String source, String destination) {
        if (!source.endsWith(".zip")) {
            throw new IllegalArgumentException("Failed: Source file name must ends with .zip.");
        }
    }

    public UnzipArgumentParser(String[] args) throws IOException {
        super(args);
    }
}
