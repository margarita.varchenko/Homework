package com.epam.rd.june2018.argumentParser;

import java.io.IOException;
import java.nio.file.*;

public class ZipArgumentParser extends ArgumentParser {

    @Override
    protected void checkExtensions(String source, String destination) {
        if (!destination.endsWith(".zip")) {
            throw new IllegalArgumentException("Failed: Destination file name must ends with .zip.");
        }
    }

    public ZipArgumentParser(String[] args) throws IOException {
        super(args);
    }
}
