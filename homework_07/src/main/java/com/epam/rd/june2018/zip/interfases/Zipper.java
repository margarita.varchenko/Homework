package com.epam.rd.june2018.zip.interfases;

import java.io.IOException;

public interface Zipper {
    void zip(String sourceName, String destinationName) throws IOException;
}
