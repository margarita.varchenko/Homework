package com.epam.rd.june2018;

import com.epam.rd.june2018.argumentParser.ArgumentParser;
import com.epam.rd.june2018.argumentParser.UnzipArgumentParser;
import com.epam.rd.june2018.argumentParser.ZipArgumentParser;
import com.epam.rd.june2018.zip.impl.MyUnzipper;
import com.epam.rd.june2018.zip.impl.MyZipper;

import java.io.*;
import java.nio.file.*;
import java.util.Arrays;

public class App {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println("Illegal arguments: Command must starts with 'zip' or 'unzip'.");
            return;
        }

        ArgumentParser parser = null;
        try {
            switch (args[0]) {

                case "zip":
                    parser = new ZipArgumentParser(Arrays.copyOfRange(args, 1, args.length));
                    MyZipper zipper = new MyZipper();
                    zipper.zip(parser.getSourceName(), parser.getDestinationName());
                    System.out.println("Zipping is done");
                    break;

                case "unzip":
                    parser = new UnzipArgumentParser(Arrays.copyOfRange(args, 1, args.length));
                    MyUnzipper unzipper = new MyUnzipper();
                    unzipper.unzip(parser.getSourceName(), parser.getDestinationName());
                    System.out.println("Unzipping is done.");
                    break;

                default:
                    throw new IllegalArgumentException("Illegal arguments: Command must starts with 'zip' or 'unzip'.");
            }
        }

        catch (IllegalArgumentException | FileAlreadyExistsException | NoSuchFileException ex) {
            System.out.println(ex.getMessage());
        }
        catch (IOException ex) {
            System.out.println("Failed to zip file.");
            System.out.println(ex.getMessage());
        }

    }


}
