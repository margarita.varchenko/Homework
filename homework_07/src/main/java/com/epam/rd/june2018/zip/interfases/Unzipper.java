package com.epam.rd.june2018.zip.interfases;

import java.io.IOException;

public interface Unzipper {
    void unzip(String sourceZip, String destinationFolder) throws IOException;
}
