package com.epam.rd.june2018.argumentParser;

import java.nio.file.*;

public abstract class ArgumentParser {
    protected String sourceName;
    protected String destinationName;

    public String getSourceName() {
        return sourceName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    protected abstract void checkExtensions(String source, String destination);

    public ArgumentParser(String[] args) throws NoSuchFileException, FileAlreadyExistsException {
        if (args.length < 2 || args.length > 3) {
            throw new IllegalArgumentException("Illegal arguments: unacceptable number of arguments.");
        }

        Path sourcePath = Paths.get(args[0]);
        if (!Files.exists(sourcePath)) {
            throw new NoSuchFileException(args[0]);
        }
        checkExtensions(args[0], args[1]);
        sourceName = args[0];
        destinationName = args[1];

        checkDestinationExists(args);
    }

    private void checkDestinationExists(String[] args) throws FileAlreadyExistsException {
        boolean destExists = Files.exists(Paths.get(args[1]));
        if (destExists) {
            if (args.length < 3 || !args[2].equals("-r")) {
                throw new FileAlreadyExistsException(args[1], null, "Failed. Destination already exists.");
            }
        }
    }
}
