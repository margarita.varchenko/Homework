package com.epam.rd.june2018.argumentParser;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

import static org.junit.Assert.*;

public class ZipArgumentParserTest {
    ArgumentParser parser;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Test
    public void destinationExistsExceptionTest() throws IOException {
        // GIVEN
        expectedException.expect(FileAlreadyExistsException.class);

        // WHEN
        parser = new ZipArgumentParser(new String[] {"FolderForZip", "Existing.zip"});
    }

    @Test
    public void acceptDestinationExists() throws IOException {
        // GIVEN
        String source = "FolderForZip",
                dest = "Existing.zip";

        // WHEN
        parser = new ZipArgumentParser(new String[] {source, dest, "-r" });

        // THEN
        Assert.assertNotNull(parser);
        Assert.assertEquals(source, parser.getSourceName());
        Assert.assertEquals(dest, parser.getDestinationName());
    }
}