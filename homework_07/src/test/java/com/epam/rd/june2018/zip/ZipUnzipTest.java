package com.epam.rd.june2018.zip;

import com.epam.rd.june2018.zip.impl.MyUnzipper;
import com.epam.rd.june2018.zip.impl.MyZipper;
import com.epam.rd.june2018.zip.interfases.Unzipper;
import com.epam.rd.june2018.zip.interfases.Zipper;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class ZipUnzipTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private Zipper zipper = new MyZipper();
    private Unzipper unzipper = new MyUnzipper();
    @Test
    public void zipFileTest() throws IOException {
        // GIVEN
        String sourceName = "FolderForZip";
        String destName = "FolderForZip.zip";
        long expectedSize = 0;
        List<Path> files = Files.walk(Paths.get(sourceName)).collect(Collectors.toList());
        for (Path file:files) {
            expectedSize += Files.size(file);
        }

        // WHEN
        zipper.zip(sourceName, destName);
        unzipper.unzip(destName, sourceName);
        // THEN
        boolean zipExists = Files.exists(Paths.get("zippedFileForZip.zip"));

        Assert.assertEquals(true, zipExists);
        Assert.assertEquals(expectedSize, Files.size(Paths.get(sourceName)));
    }

    @Test
    public void sourceNotExistZipTest() throws IOException {
        // GIVEN
        expectedException.expect(NoSuchFileException.class);
        //WHEN
        zipper.zip("nonExistingFolder", "nonExistingFolder.zip");

    }

    @Test
    public void sourceNotExistUnzipTest() throws IOException {
        // GIVEN
        expectedException.expect(FileNotFoundException.class);
        //WHEN
        unzipper.unzip("nonExistingFolder.zip", "nonExistingFolder");

    }
}