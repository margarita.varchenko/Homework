The program needs arguments:
zip/unzip <source> <destination> [-r]

'-r' is needed to override destination if it already exists.