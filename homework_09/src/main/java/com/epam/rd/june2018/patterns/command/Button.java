package com.epam.rd.june2018.patterns.command;

import com.epam.rd.june2018.patterns.command.commands.Command;

public class Button {

    private Command command;
    private CommandStack commandStack;

    public Button(CommandStack commandStack) {
        this.commandStack = commandStack;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void executeCommand() {
        if (command.execute()) {
            commandStack.push(command);
        }
    }
}
