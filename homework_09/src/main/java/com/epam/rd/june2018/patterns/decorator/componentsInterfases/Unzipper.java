package com.epam.rd.june2018.patterns.decorator.componentsInterfases;

import java.io.IOException;

public interface Unzipper {
    void unzip(String sourceZip, String destinationFolder) throws IOException;
}
