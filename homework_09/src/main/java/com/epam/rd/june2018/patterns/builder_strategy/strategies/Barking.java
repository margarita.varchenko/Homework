package com.epam.rd.june2018.patterns.builder_strategy.strategies;

public class Barking implements SoundBehavior {
    public void makeSound() {
        System.out.println("Woof");
    }
}