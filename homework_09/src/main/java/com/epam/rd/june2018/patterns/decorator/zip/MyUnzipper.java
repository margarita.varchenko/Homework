package com.epam.rd.june2018.patterns.decorator.zip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Unzipper;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.*;

public class MyUnzipper implements Unzipper {

    public void unzip(String sourceZip, String destinationFolder) throws IOException {

        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(sourceZip))) {

            ZipEntry zipEntry = zipInputStream.getNextEntry();

            while (zipEntry != null) {
                unzipFile(destinationFolder, zipEntry, zipInputStream);
                zipEntry = zipInputStream.getNextEntry();
            }

            zipInputStream.closeEntry();
        }
    }

    private void unzipFile(String destinationFolder, ZipEntry zipEntry, ZipInputStream zipInputStream) throws IOException {
        Path entryPath = Paths.get(destinationFolder, zipEntry.getName());
        Path entryFolder = entryPath.subpath(0, entryPath.getNameCount() - 1);
        Files.createDirectories(entryFolder);
        byte[] buffer = new byte[1024];

        try (FileOutputStream fos = new FileOutputStream(entryPath.toString())) {
            int len;
            while ((len = zipInputStream.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
        }

    }
}
