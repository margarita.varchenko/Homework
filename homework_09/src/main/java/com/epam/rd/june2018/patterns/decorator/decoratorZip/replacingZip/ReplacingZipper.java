package com.epam.rd.june2018.patterns.decorator.decoratorZip.replacingZip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Zipper;
import com.epam.rd.june2018.patterns.decorator.decoratorZip.ZipperDecorator;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReplacingZipper extends ZipperDecorator {

    private boolean replaceDestination;

    public ReplacingZipper(Zipper zipper, boolean replaceDestination) {
        this.zipper = zipper;
        this.replaceDestination = replaceDestination;
    }

    public void zip(String sourceName, String destinationName) throws IOException {

        if (replaceDestination) {
            Path destPath = Paths.get(destinationName);
            if (Files.exists(destPath)) {
                throw new FileAlreadyExistsException(destinationName);
            }
        }

        zipper.zip(sourceName, destinationName);

    }
}