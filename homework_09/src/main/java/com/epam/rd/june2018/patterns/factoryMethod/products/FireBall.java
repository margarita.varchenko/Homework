package com.epam.rd.june2018.patterns.factoryMethod.products;

public class FireBall extends Spell {

    public FireBall() {
        spellName = "Fireball";
        manaPoints = 5;
        timeForCast = 2;
        range = 7;
    }
}
