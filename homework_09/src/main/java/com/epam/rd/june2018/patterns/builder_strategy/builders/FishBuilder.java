package com.epam.rd.june2018.patterns.builder_strategy.builders;

import com.epam.rd.june2018.patterns.builder_strategy.products.Fish;
import com.epam.rd.june2018.patterns.builder_strategy.strategies.Silence;

public class FishBuilder extends PetBuilder {
    private Fish fish;

    public Fish getProduct() {
        return fish;
    }

    public FishBuilder() {
        fish = new Fish();
    }

    @Override
    public PetBuilder buildFavoriteFood() {
        fish.setFavoriteFood("Seaweed");
        return this;
    }

    @Override
    public PetBuilder buildTimeBetweenMeals() {
        fish.setTimeBetweenMeals(12);
        return this;
    }

    @Override
    public PetBuilder buildSoundBehavior() {
        fish.setSoundBehavior(new Silence());
        return this;
    }

}
