The usage of the pattern is illustrated by part of a game in which
a player creates some creature and plays by it. This illustration
represents the creation of a creature.
For this creation there is a user interface with buttons like
"Increase/Decrease size of creature", "Change consumer type" etc.
For every change of the creature there is a Command object and
by clicking the needed button the player invoke executing of the
corresponding command.