package com.epam.rd.june2018.patterns.factoryMethod.products;

public abstract class Spell {
    protected String spellName;
    protected int manaPoints;
    protected int timeForCast;
    protected int range;

    public String getSpellName() {
        return spellName;
    }
}
