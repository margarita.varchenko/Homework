package com.epam.rd.june2018.patterns.command.commands;

import com.epam.rd.june2018.patterns.command.Creature;

public class IncreaseSize implements Command {
    private Creature creature;

    public IncreaseSize(Creature creature) {
        this.creature = creature;
    }


    public boolean execute() {
        creature.setSize(creature.getSize() + 1);
        return true;
    }

    public void undo() {
        creature.setSize(creature.getSize() - 1);
    }
}
