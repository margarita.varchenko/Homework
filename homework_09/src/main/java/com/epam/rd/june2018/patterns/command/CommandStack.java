package com.epam.rd.june2018.patterns.command;

import com.epam.rd.june2018.patterns.command.commands.Command;

import java.util.Stack;

public class CommandStack {
    private Stack<Command> commandStack;

    public CommandStack() {
        commandStack = new Stack<>();
    }

    public void push(Command command) {
        commandStack.push(command);
    }

    public Command pop() {
        return commandStack.pop();
    }
}
