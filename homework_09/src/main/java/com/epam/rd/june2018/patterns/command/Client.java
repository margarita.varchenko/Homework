package com.epam.rd.june2018.patterns.command;

import com.epam.rd.june2018.patterns.command.commands.*;

public class Client {
    public static void main(String[] args) {
        Creature creature = new Creature();
        CommandStack commandStack = new CommandStack();

        // attach commands to buttons
        Button changeConsumerBtn = new Button(commandStack);
        changeConsumerBtn.setCommand(new ChangeConsumerType(creature));

        Button increaseSizeBtn = new Button(commandStack);
        increaseSizeBtn.setCommand(new IncreaseSize(creature));

        Button decreaseSizeBtn = new Button(commandStack);
        decreaseSizeBtn.setCommand(new DecreaseSize(creature));

        Button changeTeethBtn = new Button(commandStack);
        changeTeethBtn.setCommand(new ChangePoisonousTeeth(creature));

        Button undoBtn = new Button(commandStack);
        undoBtn.setCommand(new Undo(commandStack));


        System.out.println(creature);

        // clicking buttons
        changeConsumerBtn.executeCommand();
        System.out.println(creature);

        increaseSizeBtn.executeCommand();
        increaseSizeBtn.executeCommand();
        System.out.println(creature);

        undoBtn.executeCommand();
        System.out.println(creature);

        changeTeethBtn.executeCommand();
        System.out.println(creature);

        undoBtn.executeCommand();
        System.out.println(creature);

    }
}
