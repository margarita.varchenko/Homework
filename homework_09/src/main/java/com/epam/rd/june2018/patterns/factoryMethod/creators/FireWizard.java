package com.epam.rd.june2018.patterns.factoryMethod.creators;

import com.epam.rd.june2018.patterns.factoryMethod.products.FireBall;
import com.epam.rd.june2018.patterns.factoryMethod.products.Spell;

public class FireWizard extends SpellCaster {


    public Spell createSpell() {
        return new FireBall();
    }


}
