Now I realize that it was not a good decision I've made in the homework_07 (zipper/unzipper).
There I have a class Zipper/Unzipper with one method (zip/unzip).
And this method does not have mechanism to avoid replacement of existing destination
so client need to check if destination exists by himself.
And now I also realize that I don't have to change existing code,
I can create a decorator for it.