package com.epam.rd.june2018.patterns.factoryMethod.creators;

import com.epam.rd.june2018.patterns.factoryMethod.products.FreezingSpell;
import com.epam.rd.june2018.patterns.factoryMethod.products.Spell;

public class IceWizard extends SpellCaster {


    public Spell createSpell() {
        return new FreezingSpell();
    }


}
