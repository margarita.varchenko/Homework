package com.epam.rd.june2018.patterns.factoryMethod.products;

public class FreezingSpell extends Spell {

    public FreezingSpell() {
        spellName = "Freezing spell";
        manaPoints = 7;
        timeForCast = 4;
        range = 4;
    }
}
