package com.epam.rd.june2018.patterns.factoryMethod.creators;

import com.epam.rd.june2018.patterns.factoryMethod.products.Spell;

public abstract class SpellCaster {


    public abstract Spell createSpell();


    public void castSpell() {
        Spell spellForCast = createSpell();
        System.out.println("Cast the spell " + spellForCast.getSpellName() + "to the opponent!");
    }

}
