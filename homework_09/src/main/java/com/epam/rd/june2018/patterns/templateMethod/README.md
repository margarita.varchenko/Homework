There is a part from homework_06 (databases).
Here I have AbstractDAO class whose non-abstract methods
(like insert(), getByKey(), update() and delete()) implements steps
that are common for all DAO classes.
Steps that differ for different classes
(like parsing the results of a query or getting string for a query)
are abstract.