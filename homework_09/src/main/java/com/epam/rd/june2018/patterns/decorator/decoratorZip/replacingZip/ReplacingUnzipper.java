package com.epam.rd.june2018.patterns.decorator.decoratorZip.replacingZip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Unzipper;
import com.epam.rd.june2018.patterns.decorator.decoratorZip.UnzipperDecorator;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReplacingUnzipper extends UnzipperDecorator {

    private boolean replaceDestination;

    public ReplacingUnzipper(Unzipper unzipper, boolean replaceDestination) {
        this.unzipper = unzipper;
        this.replaceDestination = replaceDestination;
    }

    public void unzip(String sourceZip, String destinationFolder) throws IOException {

        if (replaceDestination) {
            Path destPath = Paths.get(destinationFolder);
            if (Files.exists(destPath)) {
                throw new FileAlreadyExistsException(destinationFolder);
            }
        }

        unzipper.unzip(sourceZip, destinationFolder);

    }
}
