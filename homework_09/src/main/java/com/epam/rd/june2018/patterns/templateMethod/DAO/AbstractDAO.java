package com.epam.rd.june2018.patterns.templateMethod.DAO;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

/*
E is entity type, K is key type
 */
public abstract class AbstractDAO<E,K> {

    public Connection getConnection() throws SQLException, IOException {
        Properties properties = new Properties();
        try (InputStream in =
                     AbstractDAO.class.getClassLoader().getResourceAsStream("db\\db.properties")) {
            properties.load(in);
        }

        String drivers = properties.getProperty("db.driver");
        if (drivers != null) {
            System.setProperty("db.driver", drivers);
        }
        String url = properties.getProperty("db.url");
        String username = properties.getProperty("db.user");
        String password = properties.getProperty("db.password");

        return DriverManager.getConnection(url, username, password);
    }

    protected abstract String getSelectByKeyQuery();

    protected abstract String getInsertQuery();

    protected abstract String getUpdateQuery();

    protected abstract String getDeleteQuery();

    protected abstract void prepareSelectByKeyStatement(PreparedStatement statement, K key) throws SQLException;

    protected abstract void prepareInsertStatement(PreparedStatement statement, E newItem) throws SQLException;

    protected abstract void prepareUpdateStatement(PreparedStatement statement, E updatedItem) throws SQLException;

    protected abstract void prepareDeleteStatement(PreparedStatement statement, K key) throws SQLException;

    protected abstract E getNextFromResultSet(ResultSet resultSet) throws SQLException;

    protected abstract K getActualKey(E insertedItem, PreparedStatement statement) throws SQLException;

    public E getByKey(K key) throws SQLException, IOException {
        String selectByIdQuery = getSelectByKeyQuery();
        try (Connection connection = getConnection()) {

            PreparedStatement statement = connection.prepareStatement(selectByIdQuery);
            prepareSelectByKeyStatement(statement, key);
            ResultSet resSet = statement.executeQuery();

            if (resSet.next()) {
                return getNextFromResultSet(resSet);
            } else {
                throw new NoSuchElementException();
            }
        }

    }

    /*
    Inserts newItem in the database and return actual value of primary key
    of the inserted object in the database (needed if the PK is autoincrement)
     */
    public K insert(E newItem) throws SQLException, IOException {
        String insertQuery = getInsertQuery();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            prepareInsertStatement(statement, newItem);
            statement.executeUpdate();
            return getActualKey(newItem, statement);
        }
    }

    public void update(E updatedItem) throws SQLException, IOException {
        String updateQuery = getUpdateQuery();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            prepareUpdateStatement(statement, updatedItem);
            statement.executeUpdate();
        }
    }

    public void delete(K key) throws SQLException, IOException {
        String deleteQuery = getDeleteQuery();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            prepareDeleteStatement(statement, key);
            statement.executeUpdate();
        }
    }


    protected java.sql.Date convert(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }
}
