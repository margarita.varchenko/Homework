package com.epam.rd.june2018.patterns.builder_strategy.strategies;

public class Silence implements SoundBehavior {
    public void makeSound() { }
}
