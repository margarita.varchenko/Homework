package com.epam.rd.june2018.patterns.builder_strategy;

import com.epam.rd.june2018.patterns.builder_strategy.builders.BirdBuilder;
import com.epam.rd.june2018.patterns.builder_strategy.builders.FishBuilder;
import com.epam.rd.june2018.patterns.builder_strategy.builders.PuppyBuilder;
import com.epam.rd.june2018.patterns.builder_strategy.products.Bird;
import com.epam.rd.june2018.patterns.builder_strategy.products.Fish;
import com.epam.rd.june2018.patterns.builder_strategy.products.Pet;
import com.epam.rd.june2018.patterns.builder_strategy.products.Puppy;

public class Client {
    public static void main(String[] args) {
        FishBuilder fishBuilder = new FishBuilder();
        Director director = new Director(fishBuilder);
        director.construct();

        Fish builtFish = fishBuilder.getProduct();

        BirdBuilder birdBuilder = new BirdBuilder();
        director.setBuilder(birdBuilder);
        director.construct();

        Bird builtBird = birdBuilder.getProduct();

        PuppyBuilder puppyBuilder = new PuppyBuilder();
        director.setBuilder(puppyBuilder);
        director.construct();

        Puppy builtPuppy = puppyBuilder.getProduct();

        Pet[] pets = new Pet[] {builtBird, builtFish, builtPuppy};
        for (Pet pet:pets) {
            pet.makeSound();
        }
    }
}
