This realization represents objects of some game.
There is object "spell" that corresponds to the "product" in Factory Method Pattern.
Spells are created by SpellCasters ("creator").
There are few kinds of spells (like FireSpell, FreezingSpell) that are "concrete product".
"Concrete creators" are represented by concrete spell casters (FireWizard, IceWizard).