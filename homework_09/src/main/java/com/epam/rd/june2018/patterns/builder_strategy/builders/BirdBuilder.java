package com.epam.rd.june2018.patterns.builder_strategy.builders;

import com.epam.rd.june2018.patterns.builder_strategy.products.Bird;
import com.epam.rd.june2018.patterns.builder_strategy.strategies.Tweeting;

public class BirdBuilder extends PetBuilder {
    private Bird bird;

    public Bird getProduct() {
        return bird;
    }

    public BirdBuilder() {
        bird = new Bird();
    }

    @Override
    public PetBuilder buildFavoriteFood() {
        bird.setFavoriteFood("Grains");
        return this;
    }

    @Override
    public PetBuilder buildTimeBetweenMeals() {
        bird.setTimeBetweenMeals(12);
        return this;
    }

    @Override
    public PetBuilder buildSoundBehavior() {
        bird.setSoundBehavior(new Tweeting());
        return this;
    }


}