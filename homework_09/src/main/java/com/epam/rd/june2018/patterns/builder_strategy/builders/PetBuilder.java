package com.epam.rd.june2018.patterns.builder_strategy.builders;

public abstract class PetBuilder {

    public PetBuilder buildFavoriteFood() {
        return this;
    }

    public PetBuilder buildTimeBetweenMeals() {
        return this;
    }

    public PetBuilder buildSoundBehavior() {
        return this;
    }
}
