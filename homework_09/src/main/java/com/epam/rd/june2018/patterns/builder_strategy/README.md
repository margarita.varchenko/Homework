There are two patterns (builder and strategy) represented by game
in which you need to take care of animals in pet store.
The products are represented by classes that extends abstract Pet class (fish, bird, puppy).
The abstract class PetBuilder declares methods that create parts of pet objects.
Concrete builders (FishBuilder, BirdBuilder, PuppyBuilder) extend the AnimalBuilder class
and override needed method so each pet has its own food habits and sound it makes.
There is also Director class that forms a pet for client using needed builder.