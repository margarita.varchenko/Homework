package com.epam.rd.june2018.patterns.command.commands;

import com.epam.rd.june2018.patterns.command.Creature;

public class ChangePoisonousTeeth implements Command {
    private Creature creature;

    public ChangePoisonousTeeth(Creature creature) {
        this.creature = creature;
    }

    public boolean execute() {
        creature.setPoisonousTeeth(!creature.hasPoisonousTeeth());
        return true;
    }

    public void undo() {
        creature.setPoisonousTeeth(!creature.hasPoisonousTeeth());
    }
}
