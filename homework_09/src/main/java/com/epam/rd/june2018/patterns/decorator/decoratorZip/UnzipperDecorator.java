package com.epam.rd.june2018.patterns.decorator.decoratorZip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Unzipper;

public abstract class UnzipperDecorator implements Unzipper {
    protected Unzipper unzipper;
}
