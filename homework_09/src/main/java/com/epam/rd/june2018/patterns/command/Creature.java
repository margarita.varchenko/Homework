package com.epam.rd.june2018.patterns.command;

public class Creature {

    private int size;
    private String consumerType;
    private boolean poisonousTeeth;


    public Creature() {
        size = 5;
        consumerType = "Herbivore";
        poisonousTeeth = false;
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if (size > 0 && size <= 10) {
            this.size = size;
        }
    }


    public String getConsumerType() {
        return consumerType;
    }

    public void setConsumerType(String consumerType) {
        this.consumerType = consumerType;
    }


    public boolean hasPoisonousTeeth() {
        return poisonousTeeth;
    }

    public void setPoisonousTeeth(boolean poisonousTeeth) {
        this.poisonousTeeth = poisonousTeeth;
    }

    @Override
    public String toString() {
        return "Creature\nsize: " + size
                + "\nconsumer type: " + consumerType
                + "\nhas poisonous teeth: " + poisonousTeeth + "\n\n";
    }
}
