package com.epam.rd.june2018.patterns.builder_strategy.strategies;

public interface SoundBehavior {
    void makeSound();
}
