package com.epam.rd.june2018.patterns.decorator.decoratorZip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Zipper;

public abstract class ZipperDecorator implements Zipper{
    protected Zipper zipper;
}
