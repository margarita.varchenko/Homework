package com.epam.rd.june2018.patterns.builder_strategy.builders;

import com.epam.rd.june2018.patterns.builder_strategy.products.Puppy;
import com.epam.rd.june2018.patterns.builder_strategy.strategies.Barking;

public class PuppyBuilder extends PetBuilder {
    private Puppy puppy;

    public Puppy getProduct() {
        return puppy;
    }

    public PuppyBuilder() {
        puppy = new Puppy();
    }

    @Override
    public PetBuilder buildFavoriteFood() {
        puppy.setFavoriteFood("Bone");
        return this;
    }

    @Override
    public PetBuilder buildTimeBetweenMeals() {
        puppy.setTimeBetweenMeals(8);
        return this;
    }

    @Override
    public PetBuilder buildSoundBehavior() {
        puppy.setSoundBehavior(new Barking());
        return this;
    }
}
