package com.epam.rd.june2018.patterns.decorator.decoratorZip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Zipper;
import com.epam.rd.june2018.patterns.decorator.decoratorZip.replacingZip.ReplacingZipper;
import com.epam.rd.june2018.patterns.decorator.zip.MyZipper;

import java.io.IOException;

public class Client {
    // here is example of using ReplacingZip
    public static void main(String[] args) {
        Zipper replacingZipper = new MyZipper();
        replacingZipper = new ReplacingZipper(replacingZipper, true);

        try {
            replacingZipper.zip("FolderForZip", "FolderForZip.zip");
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }
}
