package com.epam.rd.june2018.patterns.templateMethod.entities;

public class Student {
    private String email;
    private String name;
    private String password;

    public Student(String studentEmail, String studentName, String studentPassword) {
        name = studentName;
        email = studentEmail;
        password = studentPassword;
    }

    public Student() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
