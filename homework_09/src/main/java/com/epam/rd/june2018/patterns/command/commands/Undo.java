package com.epam.rd.june2018.patterns.command.commands;

import com.epam.rd.june2018.patterns.command.CommandStack;

public class Undo implements Command {
    private CommandStack commandStack;

    public Undo(CommandStack commandStack) {
        this.commandStack = commandStack;
    }

    public boolean execute() {
        commandStack.pop().undo();
        return false;
    }
    public void undo() {}
}
