package com.epam.rd.june2018.patterns.decorator.zip;

import com.epam.rd.june2018.patterns.decorator.componentsInterfases.Zipper;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.*;

public class MyZipper implements Zipper {
    /*
    Zips a file or a directory with path specified by sourceName
    into .zip file specified by destinationName. Throws exception if
    sourceName file does not exist, overrides destination file if
    it already exists.
     */
    public void zip(String sourceName, String destinationName) throws IOException {
        Path sourcePath = Paths.get(sourceName);

        int sourceNameIndex = sourcePath.getNameCount() - 1;

        Stream<Path> filesStream = Files.walk(sourcePath, Integer.MAX_VALUE);
        List<Path> filesList = filesStream.filter(file -> Files.isRegularFile(file)).collect(Collectors.toList());

        try (FileOutputStream fos = new FileOutputStream(destinationName);
             ZipOutputStream zipOutputStream = new ZipOutputStream(fos)) {

            for (Path file:filesList) {
                zipFile(file, sourceNameIndex, zipOutputStream);
            }
        }
    }

    /*
    Zip a specified by filePath file using zupOutputStream.
    The parameter startPathIndex specifies the index of name from which
    the name of zipEntry starts.
     */
    private void zipFile(Path filePath, int startPathIndex, ZipOutputStream zipOutputStream) throws IOException {
        byte[] buffer = new byte[1024];
        String zipEntryName = filePath.subpath(startPathIndex, filePath.getNameCount()).toString();

        try (FileInputStream fileInputStream = new FileInputStream(filePath.toString())) {

            zipOutputStream.putNextEntry(new ZipEntry(zipEntryName));
            int length;
            while ((length = fileInputStream.read(buffer)) > 0) {
                zipOutputStream.write(buffer, 0, length);
            }
            zipOutputStream.closeEntry();
        }
    }
}
