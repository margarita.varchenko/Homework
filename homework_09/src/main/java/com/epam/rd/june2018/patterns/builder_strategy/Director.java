package com.epam.rd.june2018.patterns.builder_strategy;

import com.epam.rd.june2018.patterns.builder_strategy.builders.PetBuilder;

public class Director {


    private PetBuilder builder;

    public Director(PetBuilder builder) {
        this.builder = builder;
    }

    public void setBuilder(PetBuilder builder) {
        this.builder = builder;
    }

    public void construct() {
        builder.buildFavoriteFood()
                .buildSoundBehavior()
                .buildTimeBetweenMeals();
    }
}
