package com.epam.rd.june2018.patterns.builder_strategy.products;

import com.epam.rd.june2018.patterns.builder_strategy.strategies.SoundBehavior;

public abstract class Pet {
    private String favoriteFood;
    private int timeBetweenMeals;
    private SoundBehavior soundBehavior;

    public void makeSound() {
        soundBehavior.makeSound();
    }

    public String getFavoriteFood() {
        return favoriteFood;
    }

    public int getTimeBetweenMeals() {
        return timeBetweenMeals;
    }

    public SoundBehavior getSoundBehavior() {
        return soundBehavior;
    }

    public void setFavoriteFood(String favoriteFood) {
        this.favoriteFood = favoriteFood;
    }

    public void setSoundBehavior(SoundBehavior soundBehavior) {
        this.soundBehavior = soundBehavior;
    }

    public void setTimeBetweenMeals(int hours) {
        this.timeBetweenMeals = timeBetweenMeals;
    }
}
