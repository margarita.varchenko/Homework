package com.epam.rd.june2018.patterns.templateMethod.DAO;

import java.sql.*;
import com.epam.rd.june2018.patterns.templateMethod.entities.Course;

public class CourseDAO extends AbstractDAO<Course,Long> {

    @Override
    protected void prepareInsertStatement(PreparedStatement statement, Course newCourse) throws SQLException {
        statement.setString(1, newCourse.getTitle());
        statement.setDate(2, convert(newCourse.getStartDate()));
        statement.setInt(3, newCourse.getDurationWeeks());
    }

    @Override
    protected String getSelectByKeyQuery() {
        return "SELECT * FROM courses WHERE id = ?";
    }


    @Override
    protected String getInsertQuery() {
        return "INSERT INTO courses (title, start_date, duration_weeks) VALUES (?, ?, ?)";
    }

    @Override
    protected String getUpdateQuery() {
        return "UPDATE courses SET title = ?, start_date = ?, duration_weeks = ? WHERE id = ?";
    }

    @Override
    protected String getDeleteQuery() {
        return "DELETE FROM courses WHERE id = ?";
    }

    @Override
    protected void prepareSelectByKeyStatement(PreparedStatement statement, Long id) throws SQLException {
        statement.setLong(1, id);
    }

    @Override
    protected void prepareDeleteStatement(PreparedStatement statement, Long key) throws SQLException {
        statement.setLong(1, key);
    }

    @Override
    protected void prepareUpdateStatement(PreparedStatement statement, Course updatedItem) throws SQLException {
        statement.setString(1, updatedItem.getTitle());
        statement.setDate(2, convert(updatedItem.getStartDate()));
        statement.setInt(3, updatedItem.getDurationWeeks());
        statement.setLong(4, updatedItem.getId());
    }

    @Override
    protected Course getNextFromResultSet(ResultSet resultSet) throws SQLException {
        Course course = new Course();
        course.setId(resultSet.getInt("id"));
        course.setTitle(resultSet.getString("title"));
        course.setStartDate(resultSet.getDate("start_date"));
        course.setDurationWeeks(resultSet.getInt("duration_weeks"));
        return course;
    }

    @Override
    protected Long getActualKey(Course insertedItem, PreparedStatement statement) throws SQLException {
        ResultSet actualKey = statement.getGeneratedKeys();
        if (actualKey.next()) {
            return actualKey.getLong(1);
        }
        else {
            throw new SQLException("Failed to get actual key.");
        }
    }



}
