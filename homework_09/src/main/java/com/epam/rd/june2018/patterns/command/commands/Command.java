package com.epam.rd.june2018.patterns.command.commands;

public interface Command {


     /* executes command
     *  return true if command can be undo
     *  and false otherwise (if command do not need to be added in command stack
     *  */
    boolean execute();

    void undo();
}
