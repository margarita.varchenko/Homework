package com.epam.rd.june2018.patterns.command.commands;

import com.epam.rd.june2018.patterns.command.Creature;

public class ChangeConsumerType implements Command {

    private Creature creature;

    public ChangeConsumerType(Creature creature) {
        this.creature = creature;
    }

    public boolean execute() {

        switch (creature.getConsumerType()) {
            case "Herbivore":
                creature.setConsumerType("Omnivorous");
                break;
            case "Omnivorous":
                creature.setConsumerType("Carnivore");
                break;
            case "Carnivore":
                creature.setConsumerType("Herbivore");
                break;
        }
        return true;
    }

    public void undo() {
        switch (creature.getConsumerType()) {
            case "Herbivore":
                creature.setConsumerType("Omnivorous");
                break;
            case "Omnivorous":
                creature.setConsumerType("Carnivore");
                break;
            case "Carnivore":
                creature.setConsumerType("Herbivore");
                break;
        }

    }
}
