package com.epam.rd.june2018;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.*;

import java.beans.PropertyDescriptor;

import static org.junit.Assert.*;

public class ProcessorTest {
    Producer mock = Mockito.mock(Producer.class);

    @Mock
    private Producer producer;
    @Mock
    private Consumer consumer;

    @InjectMocks
    private Processor processor = new Processor();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testProcessor() {
        // GIVEN
        Mockito.when(producer.produce()).thenReturn("Magic Value");
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        // WHEN
        processor.process();

        // THEN

        Mockito.verify(producer, Mockito.times(1)).produce();
        Mockito.verify(consumer, Mockito.times(1)).consume(captor.capture());
        String capturedString = captor.getValue();
        assertEquals("Magic Value", capturedString);
    }

    @Test
    public void testWithIllegalStateException() {
        // GIVEN
        Mockito.when(producer.produce()).thenReturn(null);
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        expectedException.expect(IllegalStateException.class);

        // WHEN
        processor.process();

    }
}