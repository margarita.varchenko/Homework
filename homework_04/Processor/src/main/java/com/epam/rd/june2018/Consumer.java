package com.epam.rd.june2018;

public class Consumer {
    public void consume(String value) {
        System.out.println("Consumed -> " + value);
    }
}
