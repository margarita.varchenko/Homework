package com.epam.rd.june2018.lists;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class MyLinkedListTest {
    MyLinkedList<String> linkedList = new MyLinkedList<String>();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void addAndGetItems() {
        // GIVEN
        linkedList.add("zero"); // [zero]
        linkedList.add("first"); // [zero, first]
        linkedList.add(1, "new first"); // [zero, new first, first]
        linkedList.addFirst("new zero"); // [new zero, zero, new first, first]
        int expectedSize = 4;

        // WHEN
        String zero = linkedList.get(0),
                first = linkedList.get(1),
                second = linkedList.get(2),
                third = linkedList.get(3);
        // THEN
        Assert.assertNotNull(zero);
        Assert.assertNotNull(first);
        Assert.assertNotNull(second);
        Assert.assertNotNull(third);

        Assert.assertEquals("new zero", zero);
        Assert.assertEquals("zero", first);
        Assert.assertEquals("new first", second);
        Assert.assertEquals("first", third);

        Assert.assertEquals(expectedSize, linkedList.size());
    }

    @Test
    public void deleteTest() {
        // GIVEN
        linkedList.add("zero");
        linkedList.add("first");
        linkedList.add("second");
        linkedList.add("third");

        // WHEN
        int sizeAfterAdding = linkedList.size();
        String first = linkedList.delete(1),
                zero = linkedList.deleteHead(),
                third = linkedList.deleteTail(),
                second = linkedList.delete(0);

        // THEN
        Assert.assertNotNull(linkedList);
        Assert.assertEquals(0, linkedList.size());
        Assert.assertEquals("zero", zero);
        Assert.assertEquals("first", first);
        Assert.assertEquals("second", second);
        Assert.assertEquals("third", third);
    }

    @Test
    public void callGetMethodWithIllegalArgument() {
        // GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        linkedList.add("zero");
        linkedList.add("first");
        // WHEN
        linkedList.get(2);
    }

    @Test
    public void deleteFromEmptyList() {
        // GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        // WHEN
        linkedList.delete(0);
    }

    @Test
    public void deleteHeadFromEmptyList() {
        // GIVEN
        expectedException.expect(NullPointerException.class);
        // WHEN
        linkedList.deleteHead();
    }

    @Test
    public void deleteTailFromEmptyList() {
        // GIVEN
        expectedException.expect(NullPointerException.class);
        // WHEN
        linkedList.deleteTail();
    }

    @Test
    public void addWithIndexOutOfBound() {
        // GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        // WHEN
        linkedList.add(0, "zero");
    }
}