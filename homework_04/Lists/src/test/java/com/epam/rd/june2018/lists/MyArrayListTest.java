package com.epam.rd.june2018.lists;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class MyArrayListTest {
    MyArrayList<String> arrayList = new MyArrayList<String>();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void addOneItemAndGetIt() throws Exception {
        // GIVEN
        String addedString = "lalala";
        // WHEN
        arrayList.add(addedString);
        String result = arrayList.get(0);
        // THEN
        Assert.assertNotNull(result);
        Assert.assertEquals(addedString, result);
    }

    @Test
    public void addFewItems() {
        // GIVEN
        String zeroString = "zero string";

        // WHEN
        arrayList.add(zeroString); // [zero string]
        arrayList.add("first string"); // [zero string, first string]
        arrayList.add(1, "new first string"); // [zero string, new first string, first string]

        // THEN
        Assert.assertNotNull(arrayList);
        Assert.assertEquals("zero string", arrayList.get(0));
        Assert.assertEquals("new first string", arrayList.get(1));
        Assert.assertEquals("first string", arrayList.get(2));
    }

    @Test
    public void sizeWhenAddAndDelete() {
        // GIVEN
        arrayList.add("zero string");
        arrayList.add("first string");
        arrayList.add("second string");
        // WHEN
        int threeAdded = arrayList.size();

        // GIVEN
        arrayList.delete(1);
        // WHEN
        int oneDeleted = arrayList.size();

        // GIVEN
        arrayList.delete(0);
        arrayList.delete(0);
        // WHEN
        int allDeleted = arrayList.size();

        // THEN
        Assert.assertEquals(3, threeAdded);
        Assert.assertEquals(2, oneDeleted);
        Assert.assertEquals(0, allDeleted);
    }

    @Test
    public void callGetMethodWithIllegalArgument() {
        // GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        arrayList = new MyArrayList<String>(5);
        arrayList.add("zero string");
        arrayList.add("first string");
        // WHEN

        arrayList.get(2);

    }

    @Test
    public void callAddMethodWithIllegalArgument() {
        // GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        arrayList = new MyArrayList<String>(5);
        arrayList.add("zero string");
        arrayList.add("first string");
        // WHEN
        arrayList.add(5, "fifth string");

    }

    @Test
    public void setNewValue() {
        // GIVEN
        arrayList.add("zero item");
        arrayList.add("first item");
        arrayList.set(1, "updated first item");

        // WHEN
        String result = arrayList.get(1);

        // THEN
        Assert.assertEquals("updated first item", result);
    }

    @Test
    public void constructWithZeroInitialCapacity() {
        arrayList = new MyArrayList<String>(0);

        // WHEN
        int zeroSize = arrayList.size();
        arrayList.add("zero");
        String addedItem = arrayList.get(0);

        // THEN
        Assert.assertNotNull(arrayList);
        Assert.assertNotNull(addedItem);
        Assert.assertEquals(0, zeroSize);
        Assert.assertEquals(1, arrayList.size());
    }

}