package com.epam.rd.june2018.lists;

public abstract class MyList<E> {
    protected int size;

    public int size() {
        return size;
    }

    abstract void add(E item);
    abstract void add(int index, E item);
    abstract E delete(int index);
    abstract E get(int index);
}
