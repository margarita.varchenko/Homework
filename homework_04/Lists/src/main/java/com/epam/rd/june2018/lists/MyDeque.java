package com.epam.rd.june2018.lists;

public interface MyDeque<E> {
    void add(E item);
    void addFirst(E item);
    E deleteHead();
    E deleteTail();
    E getHead();
    E getTail();
}
