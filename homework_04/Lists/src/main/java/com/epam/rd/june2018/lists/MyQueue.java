package com.epam.rd.june2018.lists;

public interface MyQueue<E> {
    void add(E item);
    E deleteHead();
    E getHead();
}
