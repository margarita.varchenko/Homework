package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;
/*import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
*/

import java.util.ArrayList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class TreeMapTest {

    @Test
    void putAndGetTest() {
        // GIVEN
        Map<Integer, String> myMap = new TreeMap<>();
        myMap.put(1, "first");
        myMap.put(2, "second");

        // WHEN
        String value = myMap.get(1);
        String second = myMap.get(2);

        // THEN
        assertEquals("first", value);
        assertEquals("second", second);
    }

    @Test
    void sizeTest() {
        // GIVEN
        Map<Integer, String> myMap = new TreeMap<>();
        for (int i = 0; i < 20; i++) {
            myMap.put(i, ((Integer)i).toString());
        }

        // WHEN
        int size = myMap.size();

        // THEN
        assertEquals(20, size);
    }

    @Test
    void iteratorTest() {
        // GIVEN
        Map<Integer, String> myMap = new TreeMap<>();
        ArrayList<Integer> array = new ArrayList<>(20);
        for (int i = 0; i < 20; i++) {
            myMap.put(i, ((Integer)i).toString());
            array.add(i);
        }
        int size = 0;

        // WHEN
        for (Map.Entry<Integer,String> entry:myMap) {
            assertEquals(true, array.contains(entry.key));
            size++;
        }

        // THEN
        assertEquals(20, size);
    }

}