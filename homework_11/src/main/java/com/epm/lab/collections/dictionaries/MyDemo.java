package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

public class MyDemo {

    @Option(name="-o", aliases="--ordered", usage="Define ordered output.")
    private boolean ordered;

    @Option(name="-r", aliases="--romanToArabic", usage="Set the option if mapping from roman to arabic needed.")
    private boolean romanToArabic;

    @Option(name="-f", aliases="--numbersFile", usage="Fully qualified path and name of file with numbers (each line must be <arabic number>=<roman number>).", required=true)
    private String fileName;

    public static void main(String... args) throws Exception {
        new MyDemo().doMain(args);
    }

    private void doMain(String[] args) throws Exception {
        CmdLineParser parser = new CmdLineParser(this);

        if (args.length == 0) {
            System.out.println("Map arabic numbers to roman numbers or vice versa. Usage: ");
            parser.printUsage(System.out);
            return;
        } else {
            try {
                parser.parseArgument(args);
            } catch (CmdLineException e) {
                System.err.println(e.getMessage());
                return;
            }
        }

        Path path = Paths.get(fileName);
        if (!Files.exists(path)) {
            System.out.println("Failed: The file does not exist.");
            return;
        }

        Map<Integer, String> mapArabicToRoman = ordered ? new TreeMap<>() : new HashMap<>();
        Map<String, Integer> mapRomanToArabic = ordered ? new TreeMap<>() : new HashMap<>();


        Stream<String> stringStream = Files.lines(path);
        try {
            if (romanToArabic) {
                stringStream.forEach(line -> putRomanToArabic(line, mapRomanToArabic));
            } else {
                stringStream.forEach(line -> putArabicToRoman(line, mapArabicToRoman));
            }
        }
        catch (Exception ex) {
            System.out.println("Failed reading file. Wrong format.");
            return;
        }

        if (romanToArabic) {
            romanToArabic(mapRomanToArabic);
        } else {
            arabicToRoman(mapArabicToRoman);
        }


        System.out.println("App exit.");
    }

    static void putArabicToRoman(String line, Map<Integer, String> map) {
        String[] pair = line.split("=");
        int key = Integer.parseInt(pair[0]);
        map.put(key, pair[1]);
    }

    static void putRomanToArabic(String line, Map<String, Integer> map) {
        String[] pair = line.split("=");
        int key = Integer.parseInt(pair[0]);
        map.put(pair[1], key);
    }

    private void arabicToRoman(Map<Integer, String> map) {
        System.out.println("Success. Enter an arabic number:");
        Scanner scanner = new Scanner(System.in);
        int arabicNum;
        while (true) {
            System.out.print('>');
            try {
                arabicNum = scanner.nextInt();
            }
            catch (Exception ex) {
                break;
            }
            System.out.println(map.get(arabicNum));
        }
    }

    private void romanToArabic(Map<String, Integer> map) {
        System.out.println("Success. Enter a roman number:");
        Scanner scanner = new Scanner(System.in);
        String romanNum;
        while (true) {
            System.out.print('>');
            try {
                romanNum = scanner.nextLine();
            }
            catch (Exception ex) {
                break;
            }
            System.out.println(map.get(romanNum));
        }
    }
}
