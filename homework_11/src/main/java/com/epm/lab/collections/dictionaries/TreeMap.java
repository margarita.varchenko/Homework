package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.OrderedMap;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class TreeMap<K extends Comparable<K>, V> implements OrderedMap<K, V> {
    private class Node <A extends K, B extends V> {
        Entry<K,V> entry;

        private Node<K,V> leftChild;
        private Node<K,V> rightChild;

        public Node() {}

        Node(K key, V value) {
            entry = new Entry<>(key, value);
        }
    }

    private Node<K,V> root = null;

    private int size = 0;

    public TreeMap() {}

    public TreeMap(K key, V value) {
        root = new Node(key, value);
        size = 1;
    }

    public V get(K key) {
        Entry<K, V> entry = getEntry(key);
        return (entry == null ? null : entry.value);
    }

    private Entry<K, V> getEntry(K key) {
        Node<K,V> currentNode = root;

        while (currentNode != null) {
            int compare = key.compareTo(currentNode.entry.key);
            if (compare < 0) {
                currentNode = currentNode.leftChild;
            } else {
                if (compare > 0) {
                    currentNode = currentNode.rightChild;
                } else {
                    return currentNode.entry;
                }
            }
        }
        return null;
    }

    public void put(K key, V value) {
        if (root == null) {
            root = new Node(key, value);
            size++;
            return;
        }

        Node<K,V> currentNode = root;
        Node<K,V> parent;
        int compare;
        do {
            parent = currentNode;
            compare = key.compareTo(currentNode.entry.key);
            if (compare < 0) {
                currentNode = currentNode.leftChild;
            } else {
                if (compare > 0) {
                    currentNode = currentNode.rightChild;
                } else {
                    currentNode.entry.value = value;
                    return;
                }
            }

        } while (currentNode != null);

        if (compare < 0) {
            parent.leftChild = new Node(key, value);
        } else {
            parent.rightChild = new Node(key, value);
        }
        size++;
    }

    public int size() {
        return size;
    }

    public boolean equals(Object o) {
        if (getClass() != o.getClass())
            return false;

        TreeMap<K,V> mapObj = (TreeMap<K,V>) o;
        if (size != mapObj.size())
            return false;

        Iterator<Entry<K,V>> iter1 = iterator();
        Iterator<Entry<K,V>> iter2 = mapObj.iterator();

        while (iter1.hasNext()) {
            if (iter1.next() != iter2.next())
                return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = 0;
        for (Entry<K, V> entry : this) {
                hashCode += entry.hashCode();

        }
        return hashCode;
    }


    public Iterator<Entry<K, V>> iterator() {
        return new ThisIterator();
    }

    private class ThisIterator implements Iterator<Entry<K,V>> {

        int remains;
        Node<K,V>[] stack;
        int stackSize;

        Node<K,V> node;

        ThisIterator() {
            remains = size;
            stack = new Node[size];
            stackSize = 0;
            node = root;
        }

        @Override
        public boolean hasNext() {
            return remains > 0;
        }

        @Override
        public Entry<K, V> next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            while (stackSize != 0 || node != null) {
                if (node != null) {

                    stack[stackSize] = node;
                    stackSize++;

                    node = node.leftChild;
                }
                else {

                    node = stack[stackSize - 1];
                    stack[stackSize - 1] = null;
                    stackSize--;

                    Entry<K,V> entryToReturn = node.entry;

                    node = node.rightChild;

                    remains--;
                    return entryToReturn;
                }
            }

            throw new NoSuchElementException();
        }
    }
}
