package com.epm.lab.collections.dictionaries;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

import com.epm.lab.collections.Map;

public class HashMap<K, V> implements Map<K, V> {

    private final static class Bucket<K, V> implements Iterable<Entry<K, V>> {
        private Entry<K, V>[] entries;
        private int size;

        public Bucket() {
            entries = new Entry[1];

        }

        public Entry<K, V> getEntry(int index) {
            return entries[index];
        }

        public void putEntry(Entry<K, V> entry) {
            if (size == entries.length) {
                entries = Arrays.copyOf(entries, entries.length + 1);
            }
            entries[size] = entry;
            size++;
        }

        public Iterator<Entry<K, V>> iterator() {
            return new Iterator<Entry<K, V>>() {
                private int nextIndex = 0;

                @Override
                public boolean hasNext() {
                    return nextIndex < size;
                }

                @Override
                public Entry<K, V> next() {
                    if (hasNext()) {
                        return entries[nextIndex++];
                    } else {
                        throw new NoSuchElementException();
                    }
                }
            };
        }
    }


    private Bucket<K, V>[] bucketsArray;

    private int size;

    public HashMap() {
        bucketsArray = new Bucket[10];
        size = 0;
    }

    public HashMap(Map<K, V> map) {
        bucketsArray = new Bucket[map.size()];
        for (Entry<K, V> entry : map) {
            this.put(entry.key, entry.value);
        }
    }

    public V get(K key) {
        int index = getIndex(key);
        Bucket<K, V> bucket = bucketsArray[index];
        for (Entry<K, V> entry : bucket) {
            if (Objects.equals(key, entry.key)) {
                return entry.value;
            }
        }
        return null;
    }

    public void put(K key, V value) {
        float loadFactor = (float) size / ((float) bucketsArray.length);
        if (loadFactor >= 3.0 / 4.0) {
            resize();
        }
        put(this.bucketsArray, key, value);
        size++;
    }

    private static <K,V> void put(HashMap.Bucket<K,V>[] bucketsArray, K key, V value) {
        Entry<K, V> newEntry = new Entry<>(key, value);
        int a;
        if (key.equals("XXVIII")) {
            a = 0;
        }
        int bucketIndex = getIndex(key, bucketsArray.length);

        if (bucketsArray[bucketIndex] == null) {
            bucketsArray[bucketIndex] = new HashMap.Bucket<>();
        }
        bucketsArray[bucketIndex].putEntry(newEntry);

    }

    public int size() {
        return size;
    }

    public boolean equals(Object o) {
        if (getClass() != o.getClass())
            return false;
        HashMap<K,V> myMapObj = (HashMap<K, V>) o;
        return Arrays.equals(bucketsArray, myMapObj.bucketsArray);
    }

    public int hashCode() {
        int hashCode = 0;
        for (Bucket<K, V> entryBucket : bucketsArray) {
            for (Entry<K, V> entry : entryBucket) {
                hashCode += entry.hashCode();
            }
        }
        return hashCode;
    }

    public Iterator<Entry<K, V>> iterator() {
        return new Iterator<Entry<K, V>>() {
            private int nextBucketIndex = 0;
            private int nextEntryIndex = 0;
            private int remain = size;

            @Override
            public boolean hasNext() {
                return remain != 0;
            }

            @Override
            public Entry<K, V> next() {
                if (this.hasNext()) {
                    for (int i = nextBucketIndex; i < bucketsArray.length; i++) {
                        if (bucketsArray[nextBucketIndex] != null) {
                            for (int j = nextEntryIndex; j < bucketsArray[i].size; j++) {


                                if (bucketsArray[nextBucketIndex].getEntry(nextEntryIndex) != null) {

                                    Entry<K, V> entryToReturn = bucketsArray[nextBucketIndex].getEntry(nextEntryIndex);

                                    remain--;
                                    nextEntryIndex++;
                                    if (bucketsArray[nextBucketIndex] == null || nextEntryIndex >= bucketsArray[nextBucketIndex].size) {
                                        nextEntryIndex = 0;
                                        nextBucketIndex++;
                                    }
                                    return entryToReturn;
                                }
                            }
                        }
                        nextEntryIndex++;
                        if (bucketsArray[nextBucketIndex] == null || nextEntryIndex >= bucketsArray[nextBucketIndex].size) {
                            nextEntryIndex = 0;
                            nextBucketIndex++;
                        }
                    }
                }
                throw new NoSuchElementException();
            }
        };
    }


    private int getIndex(K key) {
        return key.hashCode() % bucketsArray.length;
    }

    private static <K> int getIndex(K key, int arrayLength) {
        int hashcode = key.hashCode();
        return (hashcode > 0 ? hashcode : -hashcode) % arrayLength;
    }

    private void resize() {
        int newLength = bucketsArray.length + (bucketsArray.length >> 1);
        Bucket<K, V>[] resizedArray = new Bucket[bucketsArray.length + newLength];

        Iterator<Entry<K, V>> iterator = this.iterator();
        Entry<K, V> entry;
        while (iterator.hasNext()) {
            entry = iterator.next();
            put(resizedArray, entry.key, entry.value);
        }

        bucketsArray = resizedArray;
    }
}
